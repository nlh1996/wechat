module gitee.com/cuckoopark/wechat

go 1.12

require (
	github.com/beevik/etree v1.1.0
	golang.org/x/crypto v0.0.0-20190701094942-4def268fd1a4
)
